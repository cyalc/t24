package com.cyalc.t24;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;


/**
 * Created by cagriyalcinsoy on 8.11.2015.
 */
public class App extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "3Omn9M0MCyO3PebV0bdf9ZCHA";
    private static final String TWITTER_SECRET = "ClBs3CxD1PDM230hHStmNfFCWm7AyUTGMY0v6cJHhUquTlzP4h";

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
        setupGraph();
        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    private void setupGraph() {
        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        component.inject(this);
    }

    public AppComponent component() {
        return component;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

}
