package com.cyalc.t24.utils;

import android.net.Uri;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class Utility {
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    public static String fixImageUrl(String url){
        return "http://" +  Uri.encode(url, ALLOWED_URI_CHARS).substring(2);
    }
}
