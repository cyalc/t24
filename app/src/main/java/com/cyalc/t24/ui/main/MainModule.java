package com.cyalc.t24.ui.main;

import com.cyalc.t24.data.repositories.NewsRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
@Module
public class MainModule {
    private MainView view;

    public MainModule(MainView view) {
        this.view = view;
    }

    @Provides
    public MainView provideView() {
        return view;
    }

    @Provides MainPresenter providePresenter(MainView mainView, NewsRepository newsRepository){
        return new MainPresenterImpl(mainView, newsRepository);
    }
}
