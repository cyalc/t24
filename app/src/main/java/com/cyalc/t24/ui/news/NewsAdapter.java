package com.cyalc.t24.ui.news;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyalc.t24.R;
import com.cyalc.t24.data.rest.pojos.NewsData;
import com.cyalc.t24.utils.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class NewsAdapter extends BaseAdapter {

    private List<NewsData> mData = new ArrayList<>();
    private Context context;
    private Picasso picasso;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    LayoutInflater inflater;

    private int pageNumber;

    public NewsAdapter(Context context, Picasso picasso) {
        this.context = context;
        this.picasso = picasso;
        this.pageNumber = 3;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public NewsData getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.list_item_news, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }


        holder.textViewTitle.setText(Html.fromHtml(mData.get(position).getTitle()));

        String url = Utility.fixImageUrl(mData.get(position).getImages().getList());
        Picasso.with(context).load(url).error(R.drawable.logo).into(holder.imageViewNews);

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.list_item_news_textView)
        TextView textViewTitle;
        @Bind(R.id.list_item_news_imageView)
        ImageView imageViewNews;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void updateList(ArrayList<NewsData> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public void updatePageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumber() {
        return this.pageNumber;
    }

    public void addMoreData(ArrayList<NewsData> mData) {
        this.mData.addAll(mData);
        notifyDataSetChanged();
    }

    public void addNewData(NewsData newsData) {
        this.mData.add(0, newsData);
        notifyDataSetChanged();
    }

    public void updateFirstTenData(ArrayList<NewsData> mData) {
        for (int i = mData.size() - 1; i >= 0; i--) {
            if (!this.mData.contains(mData.get(i))) {
                this.mData.add(0,mData.get(i));
            }
        }
    }
}
