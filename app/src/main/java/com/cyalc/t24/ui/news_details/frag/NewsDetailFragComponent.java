package com.cyalc.t24.ui.news_details.frag;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.FragmentScope;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = NewsDetailFragModule.class
)
public interface NewsDetailFragComponent {
    void inject(NewsDetailFragment fragment);
    NewsDetailFragPresenter getNewsdetailFragPresenter();
}
