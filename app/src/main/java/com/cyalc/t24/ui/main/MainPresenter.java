package com.cyalc.t24.ui.main;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public interface MainPresenter {
    void onCreate();
    void onResume();
    void onStop();
}
