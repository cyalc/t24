package com.cyalc.t24.ui.news_details.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cyalc.t24.ui.news_details.frag.NewsDetailFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsDetailPagerAdapter extends FragmentPagerAdapter {

    private List<String> idList = new ArrayList<>();

    public NewsDetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return NewsDetailFragment.newInstance(idList.get(position));
    }

    @Override
    public int getCount() {
        return idList.size();
    }

    public void updateList(ArrayList<String> idList){
        this.idList = idList;
        notifyDataSetChanged();
    }
}
