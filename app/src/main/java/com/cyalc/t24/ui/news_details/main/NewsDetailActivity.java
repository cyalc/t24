package com.cyalc.t24.ui.news_details.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.R;
import com.cyalc.t24.ui.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsDetailActivity extends BaseActivity implements NewsDetailView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.activity_newsDetails_viewPager)
    ViewPager viewPagerNews;

    private String newsId;
    private String categoryId;
    private String categoryName;

    private NewsDetailPagerAdapter viewPagerAdapterNews;
    @Inject
    NewsDetailPresenter newsDetailPresenter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        newsId = intent.getStringExtra("newsId");
        categoryId = intent.getStringExtra("categoryId");
        categoryName = intent.getStringExtra("categoryName");

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        viewPagerAdapterNews = new NewsDetailPagerAdapter(getSupportFragmentManager());
        viewPagerNews.setAdapter(viewPagerAdapterNews);

        newsDetailPresenter.getCategoryIdList(categoryId);

        getSupportActionBar().setTitle(categoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());


    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerNewsDetailComponent.builder().appComponent(appComponent).newsDetailModule(new NewsDetailModule(this)).build().inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void error(String error) {

    }

    @Override
    public void initilazeViewPager(ArrayList<String> idList) {
        viewPagerAdapterNews.updateList(idList);
        viewPagerAdapterNews.notifyDataSetChanged();
        newsDetailPresenter.setChosenItem(newsId);
    }

    @Override
    public void setChosenNews(int position) {
        viewPagerNews.setCurrentItem(position);
    }
}
