package com.cyalc.t24.ui.news_details.main;

import com.cyalc.t24.ActivityScope;
import com.cyalc.t24.AppComponent;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = NewsDetailModule.class
)
public interface NewsDetailComponent {
    void inject(NewsDetailActivity activity);
    NewsDetailPresenter getNewsdetailPresenter();
}
