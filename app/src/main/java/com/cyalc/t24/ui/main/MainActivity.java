package com.cyalc.t24.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.R;
import com.cyalc.t24.data.rest.pojos.NewsCategory;
import com.cyalc.t24.data.rest.pojos.NewsData;
import com.cyalc.t24.ui.base.BaseActivity;
import com.cyalc.t24.ui.category_news.CategoryNewsActivity;
import com.cyalc.t24.ui.news.NewsFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity implements MainView, NewsFragment.OnNewsFragmentInteractionListener, AdapterView.OnItemSelectedListener {

    Toolbar toolbar;
    @Inject
    MainPresenter mainPresenter;

    @Bind(R.id.activity_main_spinner_categories)
    Spinner spinnerCategories;

    private ArrayList<NewsData> lastNewsData = new ArrayList<>();
    private ArrayList<NewsData> nextNewsData = new ArrayList<>();
    ArrayAdapter<NewsCategory> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        mainPresenter.onCreate();
        spinnerCategories.setEnabled(false);
        spinnerCategories.setOnItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spinnerCategories.setSelection(0);
        //mainPresenter.onResume();
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerMainComponent.builder().appComponent(appComponent).mainModule(new MainModule(this)).build().inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onStop() {
        mainPresenter.onStop();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void enableSplash() {
        toolbar.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_main_fl_container, SplashFragment.newInstance()).commit();
    }

    @Override
    public void disableSplash() {
        toolbar.setVisibility(View.VISIBLE);
        //ayrı callback içine al

        getSupportFragmentManager().beginTransaction().replace(R.id.content_main_fl_container, NewsFragment.newInstance(lastNewsData, nextNewsData)).commit();
    }

    @Override
    public void onError(String error) {
        new AlertDialog.Builder(this).setMessage(error).setTitle("Uyarı").setNegativeButton("Tamam", null).create().show();
    }

    @Override
    public void onLastNewsDataArrived(ArrayList<NewsData> data) {
        lastNewsData = data;
    }

    @Override
    public void onNextNewsDataArrived(ArrayList<NewsData> data) {
        nextNewsData = data;
    }

    @Override
    public void fillCategorySpinner(ArrayList<NewsCategory> newsCategories) {

        newsCategories.add(0, new NewsCategory("-1", "Kategoriler", "kategoriler"));
        arrayAdapter = new ArrayAdapter<>(this, R.layout.spinner_text_view, newsCategories);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(arrayAdapter);
        spinnerCategories.setEnabled(true);

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0)
            startActivity(new Intent(this, CategoryNewsActivity.class).putExtra("categoryId", arrayAdapter.getItem(position).getId()).putExtra("categoryName", arrayAdapter.getItem(position).getName()));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
