package com.cyalc.t24.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cyalc.t24.App;
import com.cyalc.t24.AppComponent;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(App.get(this).component());
    }

    protected abstract void setupComponent(AppComponent appComponent);
}

