package com.cyalc.t24.ui.category_news;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface CategoryNewsPresenter {
    void getNewsByCategoryId(String id);
}
