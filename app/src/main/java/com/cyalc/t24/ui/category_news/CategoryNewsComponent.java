package com.cyalc.t24.ui.category_news;

import com.cyalc.t24.ActivityScope;
import com.cyalc.t24.AppComponent;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = CategoryNewsModule.class
)
public interface CategoryNewsComponent {
    void inject(CategoryNewsActivity activity);
    CategoryNewsPresenter getCategoryNewsPresenter();
}
