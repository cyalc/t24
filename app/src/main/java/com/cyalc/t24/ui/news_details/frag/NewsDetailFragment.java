package com.cyalc.t24.ui.news_details.frag;


import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.R;
import com.cyalc.t24.ui.base.BaseFragment;
import com.cyalc.t24.utils.Utility;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewsDetailFragment extends BaseFragment implements NewsDetailFragView {

    @Bind(R.id.fragment_newsDetail_imageView_news)
    ImageView imageViewNews;
    @Bind(R.id.fragment_newsDetail_textView_newsTitle)
    TextView textViewTitle;
    @Bind(R.id.fragmen_newsDetail_textView_detail)
    TextView textViewDetail;
    @Bind(R.id.fragment_newsDetail_progressBar)
    ProgressBar progressBar;
    @Bind(R.id.fragment_newsDetail_fl)
    FrameLayout flProgress;

    @Inject
    Picasso picasso;

    @Inject
    NewsDetailFragPresenter newsDetailFragPresenter;

    ShareDialog shareDialog;


    private String id = "";
    private String title = "";
    private String url = "";

    public NewsDetailFragment() {
        // Required empty public constructor
    }


    public static NewsDetailFragment newInstance(String id) {
        NewsDetailFragment fragment = new NewsDetailFragment();
        Bundle args = new Bundle();
        args.putString("id", id);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString("id");
            //    mParam1 = getArguments().getString(ARG_PARAM1);
            //  mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_facebook_share){
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(title)
                        .setContentUrl(Uri.parse(url))
                        .build();

                shareDialog.show(linkContent);
            }

        } else if( item.getItemId() == R.id.action_twitter_share){
            TweetComposer.Builder builder = new TweetComposer.Builder(getContext())
                    .text(Html.fromHtml(title) + " : " + Uri.parse(url));

            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerNewsDetailFragComponent.builder().appComponent(appComponent).newsDetailFragModule(new NewsDetailFragModule(this)).build().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_detail, container, false);
        ButterKnife.bind(this, view);
        newsDetailFragPresenter.getNewsById(id);
        shareDialog = new ShareDialog(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        newsDetailFragPresenter.unsubscribe();

    }

    @Override
    public void setTitle(String title) {
        this.title = title;
        textViewTitle.setText(Html.fromHtml(title));
    }

    @Override
    public void setBody(String body) {
        textViewDetail.setText(Html.fromHtml(body));
    }

    @Override
    public void setImageSrc(String url) {
        picasso.load(Utility.fixImageUrl(url)).into(imageViewNews);
    }

    @Override
    public void showProgress() {
        flProgress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        flProgress.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);

    }

    @Override
    public void error(String error) {

    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }
}
