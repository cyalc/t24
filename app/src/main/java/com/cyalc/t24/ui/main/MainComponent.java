package com.cyalc.t24.ui.main;

import com.cyalc.t24.ActivityScope;
import com.cyalc.t24.AppComponent;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = MainModule.class
)
public interface MainComponent {
    void inject(MainActivity activity);
    MainPresenter getMainPresenter();
}
