package com.cyalc.t24.ui.category_news;

import com.cyalc.t24.data.repositories.NewsRepository;
import com.cyalc.t24.ui.news.NewsView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class CategoryNewsPresenterImpl implements CategoryNewsPresenter {

    private NewsRepository newsRepository;
    private CategoryNewsView categoryNewsView;

    public CategoryNewsPresenterImpl(NewsRepository newsRepository, CategoryNewsView categoryNewsView) {
        this.newsRepository = newsRepository;
        this.categoryNewsView = categoryNewsView;
    }

    @Override
    public void getNewsByCategoryId(String id) {
        categoryNewsView.showProgress();
        newsRepository.getNewsByCategoryId(id).filter(news -> news.isResult() == true).map(news1 -> news1.getData())
                .doOnError(throwable -> {
                    categoryNewsView.hideProgress();
                    categoryNewsView.error("Bir hata oluştu");
                })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(newsDatas -> {
                    categoryNewsView.hideProgress();
                    categoryNewsView.initializeNewsList(newsDatas);
                });

    }
}
