package com.cyalc.t24.ui.news;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cyalc.t24.R;
import com.cyalc.t24.data.rest.pojos.NewsData;
import com.cyalc.t24.ui.news_details.main.NewsDetailActivity;
import com.cyalc.t24.utils.Utility;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LastNewsFragment extends Fragment {

    private static final String LAST_NEWS_ARG = "lastnews";
    @Bind(R.id.fragment_last_news_imageView_news)
    ImageView imageViewNews;
    @Bind(R.id.fragment_last_news_textView_title)
    TextView textViewTitle;
    @Bind(R.id.fragment_last_news_fl)
    FrameLayout flWholeLayout;

    private NewsData newsData = null;



    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    public LastNewsFragment() {
    }


    public static LastNewsFragment newInstance(NewsData newsData) {
        LastNewsFragment fragment = new LastNewsFragment();
        Bundle args = new Bundle();
        args.putParcelable(LAST_NEWS_ARG, newsData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newsData = getArguments().getParcelable(LAST_NEWS_ARG);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_last_news, container, false);
        ButterKnife.bind(this, view);

        String url = Utility.fixImageUrl(newsData.getImages().getPage());

        Picasso.with(getActivity()).load(url).into(imageViewNews);
        textViewTitle.setText(Html.fromHtml(newsData.getTitle()));


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.fragment_last_news_fl)
    public void onFlClicked(){
        startActivity(new Intent(getActivity(), NewsDetailActivity.class).putExtra("newsId", (newsData.getId())).putExtra("categoryId", newsData.getCategory().getId()).putExtra("categoryName",newsData.getCategory().getName()));
    }
}
