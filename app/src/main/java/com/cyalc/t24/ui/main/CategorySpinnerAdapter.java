package com.cyalc.t24.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import com.cyalc.t24.data.rest.pojos.Category;
import com.cyalc.t24.data.rest.pojos.NewsCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class CategorySpinnerAdapter  extends ArrayAdapter {


    public CategorySpinnerAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }
}
