package com.cyalc.t24.ui.news_details.main;

import com.cyalc.t24.data.repositories.NewsRepository;
import com.cyalc.t24.data.rest.pojos.NewsDetail;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@Module
public class NewsDetailModule {
    private NewsDetailView view;

    public NewsDetailModule(NewsDetailView view) {
        this.view = view;
    }

    @Provides
    public NewsDetailView provideView() {
        return view;
    }

    @Provides
    NewsDetailPresenter providePresenter(NewsRepository newsRepository, NewsDetailView newsDetailView) {
        return new NewsDetailPresenterImpl(newsRepository, newsDetailView);
    }
}
