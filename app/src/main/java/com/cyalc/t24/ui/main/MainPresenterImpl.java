package com.cyalc.t24.ui.main;

import android.os.Handler;
import android.util.Log;

import com.cyalc.t24.data.repositories.NewsRepository;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class MainPresenterImpl implements MainPresenter {


    private MainView mainView;
    private NewsRepository newsRepository;

    private Subscription subscriptionSplash;
    public MainPresenterImpl(MainView mainView, NewsRepository newsRepository) {
        this.mainView = mainView;
        this.newsRepository = newsRepository;
    }

    @Override
    public void onCreate() {
        mainView.enableSplash();

        newsRepository.getLastNews().filter(news1 -> news1.isResult() == true).map(news -> news.getData()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> mainView.onError("Bir hata oluştu."))
                .subscribe(newsData -> {
                    mainView.onLastNewsDataArrived(newsData);
                });

        newsRepository.getNextNews(2).filter(news -> news.isResult() == true).map(news1 -> news1.getData()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> mainView.onError("Bir hata oluştu."))
                .subscribe(newsData -> {
                    mainView.onNextNewsDataArrived(newsData);
                });

        newsRepository.getCategories().filter(category -> category.isResult() == true).map(category1 -> category1.getData()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> mainView.onError("Bir hata oluştu"))
                .subscribe(newsCategories -> mainView.fillCategorySpinner(newsCategories));


      /*  Observable.interval(3, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(aLong -> {

        }); */

        subscriptionSplash = Observable.timer(1, TimeUnit.SECONDS).doOnError(throwable -> Log.e("MainPresenter", throwable.toString())).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> mainView.disableSplash());
       /* new Handler().postDelayed(() -> {
            mainView.disableSplash();
        }, 1000);*/
    }

    @Override
    public void onResume() {

    }



    @Override
    public void onStop() {
        if((subscriptionSplash!=null)&&(!subscriptionSplash.isUnsubscribed()))
            subscriptionSplash.unsubscribe();
    }
}
