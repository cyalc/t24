package com.cyalc.t24.ui.news_details.frag;

import android.util.Log;

import com.cyalc.t24.data.repositories.NewsRepository;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsDetailFragPresenterImpl implements NewsDetailFragPresenter {

    private NewsDetailFragView newsDetailFragView;
    private NewsRepository newsRepository;
    private Subscription subscription;

    public NewsDetailFragPresenterImpl(NewsDetailFragView newsDetailFragView, NewsRepository newsRepository) {
        this.newsDetailFragView = newsDetailFragView;
        this.newsRepository = newsRepository;
    }

    @Override
    public void getNewsById(String id) {
        newsDetailFragView.showProgress();

//.filter(newsSingle -> newsSingle.isResult() == true)

        subscription = newsRepository.getNewsById(id).map(newsSingle1 -> newsSingle1.getData())
                .doOnError(throwable -> {
                    newsDetailFragView.hideProgress();
                    newsDetailFragView.error("Bir hata oluştu");
                    Log.v("HEDERR", "hata : " + throwable.toString());

                })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(newsDetail -> {
                            Log.v("HEDERR", "bu  " + newsDetail.getText() + "  " + newsDetail.getTitle());
                            newsDetailFragView.setTitle(newsDetail.getTitle());
                            newsDetailFragView.setBody(newsDetail.getText());
                            newsDetailFragView.setImageSrc(newsDetail.getImages().getPage());
                            newsDetailFragView.setUrl(newsDetail.getUrls().getWeb());
                            newsDetailFragView.hideProgress();
                        }
                );
    }

    @Override
    public void unsubscribe() {
        if ((subscription != null) && (!subscription.isUnsubscribed()))
            subscription.unsubscribe();
    }
}
