package com.cyalc.t24.ui.category_news;

import com.cyalc.t24.data.rest.pojos.NewsData;

import java.util.ArrayList;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface CategoryNewsView {
    void showProgress();
    void hideProgress();
    void error(String error);
    void initializeNewsList(ArrayList<NewsData> newsDatas);
    void initializeTitle(String title);
}
