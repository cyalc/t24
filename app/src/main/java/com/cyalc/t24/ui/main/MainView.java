package com.cyalc.t24.ui.main;

import com.cyalc.t24.data.rest.pojos.NewsCategory;
import com.cyalc.t24.data.rest.pojos.NewsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public interface MainView {
    void showProgress();
    void hideProgress();
    void enableSplash();
    void disableSplash();
    void onError(String error);
    void onLastNewsDataArrived(ArrayList<NewsData> newsDatas);
    void onNextNewsDataArrived(ArrayList<NewsData> newsDatas);
    void fillCategorySpinner(ArrayList<NewsCategory> newsCategories);
}
