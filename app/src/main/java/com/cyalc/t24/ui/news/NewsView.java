package com.cyalc.t24.ui.news;

import com.cyalc.t24.data.rest.pojos.NewsData;

import java.util.ArrayList;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsView {
    void showFooterProgress();
    void hideFooterProgress();
    void addMoreData(ArrayList<NewsData> newsData);
    void error(String errorData);
    void changeViewPagerItemToNext(int toPosition);
    void updateLastNews(ArrayList<NewsData> newsDatas);
    void updateNextNews(ArrayList<NewsData> newsDatas);
    void addNewData(NewsData newsData);
}
