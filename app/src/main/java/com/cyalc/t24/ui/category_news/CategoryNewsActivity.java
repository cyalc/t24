package com.cyalc.t24.ui.category_news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.R;
import com.cyalc.t24.data.rest.pojos.NewsData;
import com.cyalc.t24.ui.base.BaseActivity;
import com.cyalc.t24.ui.news.NewsAdapter;
import com.cyalc.t24.ui.news_details.main.NewsDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CategoryNewsActivity extends BaseActivity implements CategoryNewsView {

    @Bind(R.id.toolbar_category_news)
    Toolbar toolbarCategoryNews;
    @Bind(R.id.activity_categoryNews_listView_news)
    ListView listViewNews;

    NewsAdapter newsAdapter;

    @Inject
    Picasso picasso;

    @Inject
    CategoryNewsPresenter categoryNewsPresenter;

    private String title = "";
    private String id = "-1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_news);
        ButterKnife.bind(this);
        setSupportActionBar(toolbarCategoryNews);


        Intent intent = getIntent();
        id = intent.getStringExtra("categoryId");
        title = intent.getStringExtra("categoryName");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbarCategoryNews.setNavigationOnClickListener(v -> onBackPressed());

        newsAdapter = new NewsAdapter(this, picasso);
        listViewNews.setAdapter(newsAdapter);
        categoryNewsPresenter.getNewsByCategoryId(id);
        initializeTitle(title);

        listViewNews.setOnItemClickListener((parent, view, position, id1) -> {
            startActivity(new Intent(this, NewsDetailActivity.class).putExtra("newsId", ((NewsData) parent.getItemAtPosition(position)).getId()).putExtra("categoryId", ((NewsData) parent.getItemAtPosition(position)).getCategory().getId()).putExtra("categoryName", ((NewsData) parent.getItemAtPosition(position)).getCategory().getName()));
        });
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerCategoryNewsComponent.builder().appComponent(appComponent).categoryNewsModule(new CategoryNewsModule(this)).build().inject(this);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void error(String error) {

    }

    @Override
    public void initializeNewsList(ArrayList<NewsData> newsDatas) {
        newsAdapter.updateList(newsDatas);
    }

    @Override
    public void initializeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }



}
