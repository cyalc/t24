package com.cyalc.t24.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.cyalc.t24.App;
import com.cyalc.t24.AppComponent;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public abstract class BaseFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(App.get(getActivity()).component());
    }

    protected abstract void setupComponent(AppComponent appComponent);
}

