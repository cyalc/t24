package com.cyalc.t24.ui.news;

import android.util.Log;

import com.cyalc.t24.data.repositories.NewsRepository;
import com.cyalc.t24.data.rest.NewsService;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsPresenterImpl implements NewsPresenter {

    private NewsRepository newsRepository;
    private NewsView newsView;
    private Subscription subscription;
    private Subscription subscriptionLong;
    private Subscription getSubscriptionLongNews;
    private Subscription subscriptionRefreshLast;
    private Subscription subscriptionRefreshNext;

    private int viewPagerPosition = 0;
    private int TIME_INTERVAL = 3;
    private Observable<Long> observableInterval = Observable.interval(3, TimeUnit.SECONDS).share();//interval(TIME_INTERVAL, TimeUnit.SECONDS);
    private Observable<Long> observanbleIntervalLong = Observable.interval(120, TimeUnit.SECONDS).share();
    private Observable<Long> observableIntervalLongNextNews = Observable.interval(120, TimeUnit.SECONDS).share();
    private boolean shouldStop = false;

    public NewsPresenterImpl(NewsRepository newsRepository, NewsView newsView) {
        this.newsView = newsView;
        this.newsRepository = newsRepository;
    }

    @Override
    public void onCreate() {
        newsView.hideFooterProgress();
    }

    @Override
    public void loadMoreNews(int page) {
        newsView.showFooterProgress();
        newsRepository.getNextNews(page).filter(news2 -> news2.isResult() == true).map(news1 -> news1.getData()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doOnError(throwable -> {
            newsView.hideFooterProgress();
            newsView.error("Bir hata oluştu.");
        }).subscribe(news -> {
            newsView.hideFooterProgress();
            newsView.addMoreData(news);
        });
    }

    @Override
    public void setViewPagerAutoSlide(int position) {
        viewPagerPosition = position;
        /*subscription = .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(aLong -> {

        });*/

         /* if ((viewPagerPosition + 1) != 10)
                newsView.changeViewPagerItemToNext(viewPagerPosition + 1);
            else
                newsView.changeViewPagerItemToNext(0);*/
       /* if((subscription != null)&&(!subscription.isUnsubscribed())){
            subscription.unsubscribe();
        }*/

        subscriptionLong = observanbleIntervalLong.flatMap(aLong1 -> newsRepository.getLastNews())
                .doOnError(throwable -> Log.e("t24", throwable.toString()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    newsView.updateLastNews(r.getData());
                });

        getSubscriptionLongNews = observableIntervalLongNextNews.flatMap(aLong1 -> newsRepository.getNextNews(2))
                .doOnError(throwable -> Log.e("t24", throwable.toString()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(news -> {
                    newsView.updateNextNews(news.getData());
                });

       /* getSubscriptionLongNews = observableIntervalLongNextNews.flatMap(aLong1 -> newsRepository.getLastNews()).flatMap(news -> Observable.from(news.getData())).distinct()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doOnError(throwable -> Log.e("t24",throwable.toString())).subscribe(newsData -> {
                    newsView.addNewData(newsData);
                });*/

        subscription = observableInterval.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Long>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Long aLong) {
                if (!shouldStop) {
                    if ((viewPagerPosition + 1) != 10)
                        newsView.changeViewPagerItemToNext(viewPagerPosition + 1);
                    else
                        newsView.changeViewPagerItemToNext(0);
                }
            }
        });
    }

    @Override
    public void stopViewPagerAutoSlide() {
        if ((subscription != null) && (!subscription.isUnsubscribed()))
            subscription.unsubscribe();
        if ((subscriptionLong != null) && (!subscriptionLong.isUnsubscribed()))
            subscriptionLong.unsubscribe();
        if ((getSubscriptionLongNews != null) && (!getSubscriptionLongNews.isUnsubscribed()))
            getSubscriptionLongNews.unsubscribe();
        if ((subscriptionRefreshLast != null) && (!subscriptionRefreshLast.isUnsubscribed()))
            subscriptionRefreshLast.unsubscribe();
        if ((subscriptionRefreshNext != null) && (!subscriptionRefreshNext.isUnsubscribed()))
            subscriptionRefreshNext.unsubscribe();
    }

    @Override
    public void shouldStop(boolean shouldStop) {
        this.shouldStop = shouldStop;
    }


    @Override
    public void updateViewPagerPosition(int position) {
        this.viewPagerPosition = position;
    }

    @Override
    public void refreshData() {
        subscriptionRefreshLast = newsRepository.getLastNews().doOnError(throwable -> newsView.error("Bir hata oluştu")).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(news -> newsView.updateLastNews(news.getData()));

        subscriptionRefreshNext = newsRepository.getNextNews(2).doOnError(throwable -> newsView.error("Bir hata oluştu")).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(news -> newsView.updateNextNews(news.getData()));
    }
}
