package com.cyalc.t24.ui.news_details.frag;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsDetailFragView {
    void setTitle(String title);
    void setBody(String body);
    void setImageSrc(String url);
    void showProgress();
    void hideProgress();
    void error(String error);
    void setUrl(String url);
}
