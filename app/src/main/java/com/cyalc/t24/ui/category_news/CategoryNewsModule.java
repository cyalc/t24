package com.cyalc.t24.ui.category_news;

import com.cyalc.t24.data.repositories.NewsRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@Module
public class CategoryNewsModule {
    private CategoryNewsView view;

    public CategoryNewsModule(CategoryNewsView view) {
        this.view = view;
    }

    @Provides
    public CategoryNewsView provideView() {
        return view;
    }

    @Provides CategoryNewsPresenter providePresenter(NewsRepository newsRepository, CategoryNewsView categoryNewsView){
        return new CategoryNewsPresenterImpl(newsRepository, categoryNewsView);
    }

}
