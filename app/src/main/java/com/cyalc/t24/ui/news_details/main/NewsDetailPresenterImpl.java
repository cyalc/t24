package com.cyalc.t24.ui.news_details.main;

import com.cyalc.t24.data.repositories.NewsRepository;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsDetailPresenterImpl implements NewsDetailPresenter {

    private NewsRepository newsRepository;
    private NewsDetailView newsDetailView;

    ArrayList<String> idList = new ArrayList<>();

    public NewsDetailPresenterImpl(NewsRepository newsRepository, NewsDetailView newsDetailView) {
        this.newsRepository = newsRepository;
        this.newsDetailView = newsDetailView;
    }

    @Override
    public void getCategoryIdList(String categoryId) {


        newsDetailView.showProgress();

        newsRepository.getNewsByCategoryId(categoryId).filter(news -> news.isResult() == true)
                .map(news1 -> news1.getData()).flatMap(newsDatas -> Observable.from(newsDatas)).doOnError(throwable -> {
            newsDetailView.hideProgress();
            newsDetailView.error("Bir hata oluştu");
        }).doOnNext(newsData -> idList.add(newsData.getId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newsData1 -> {
                    newsDetailView.initilazeViewPager(idList);
                });
    }

    @Override
    public void setChosenItem(String newsId) {
        newsDetailView.setChosenNews(idList.indexOf(newsId));
        newsDetailView.hideProgress();
    }
}
