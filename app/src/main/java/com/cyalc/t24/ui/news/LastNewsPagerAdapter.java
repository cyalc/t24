package com.cyalc.t24.ui.news;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cyalc.t24.data.rest.pojos.NewsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class LastNewsPagerAdapter extends FragmentStatePagerAdapter{

    private List<NewsData> mData = new ArrayList<>();

    public LastNewsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return LastNewsFragment.newInstance(mData.get(position));
    }

    @Override
    public int getCount() {
        return mData.size();
    }



    public void updateList(ArrayList<NewsData> mData){
        this.mData = mData;
        notifyDataSetChanged();
    }
}
