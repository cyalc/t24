package com.cyalc.t24.ui.news;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.FragmentScope;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = NewsModule.class
)
public interface NewsComponent {
    void inject(NewsFragment newsFragment);
    NewsPresenter getNewsPresenter();
}
