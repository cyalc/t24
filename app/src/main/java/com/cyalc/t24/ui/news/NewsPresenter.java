package com.cyalc.t24.ui.news;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsPresenter {
    void onCreate();

    void loadMoreNews(int page);

    void setViewPagerAutoSlide(int position);

    void stopViewPagerAutoSlide();

    void shouldStop(boolean shouldStop);

    void updateViewPagerPosition(int position);

    void refreshData();
}
