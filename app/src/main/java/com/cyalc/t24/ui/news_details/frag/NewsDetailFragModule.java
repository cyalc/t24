package com.cyalc.t24.ui.news_details.frag;

import com.cyalc.t24.data.repositories.NewsRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@Module
public class NewsDetailFragModule {
    private NewsDetailFragView view;

    public NewsDetailFragModule(NewsDetailFragView view) {
        this.view = view;
    }

    @Provides
    public NewsDetailFragView provideView() {
        return view;
    }

    @Provides
    NewsDetailFragPresenter providePresenter(NewsRepository newsRepository, NewsDetailFragView newsDetailFragView) {
        return new NewsDetailFragPresenterImpl(newsDetailFragView, newsRepository);
    }
}
