package com.cyalc.t24.ui.news;

import com.cyalc.t24.data.repositories.NewsRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
@Module
public class NewsModule {
    private NewsView view;

    public NewsModule(NewsView view) {
        this.view = view;
    }

    @Provides
    public NewsView provideView() {
        return view;
    }

    @Provides NewsPresenter providePresenter(NewsView view, NewsRepository newsRepository){
        return new NewsPresenterImpl(newsRepository,view);
    }
}
