package com.cyalc.t24.ui.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cyalc.t24.AppComponent;
import com.cyalc.t24.R;
import com.cyalc.t24.data.rest.pojos.NewsData;
import com.cyalc.t24.ui.base.BaseFragment;
import com.cyalc.t24.ui.news_details.main.NewsDetailActivity;
import com.cyalc.t24.utils.OnSwipeTouchListener;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewsFragment extends BaseFragment implements NewsView, AbsListView.OnScrollListener {

    @Bind(R.id.fragment_news_listView_news)
    ListView listViewNews;
    private ArrayList<NewsData> lastNews;
    private ArrayList<NewsData> nextNews;

    private static final String LAST_NEWS_ARG = "lastnews";
    private static final String NEXT_NEWS_ARG = "nextnews";

    private OnNewsFragmentInteractionListener mListener;

    private NewsAdapter newsAdapter;

    private LastNewsPagerAdapter lastNewsPagerAdapter;
    private ViewPager viewPagerLastNews;
    private CirclePageIndicator circlePageIndicator;

    protected boolean loading = false;


    private TextView textViewFooter;
    @Inject
    Picasso picasso;
    @Inject
    NewsPresenter newsPresenter;


    public NewsFragment() {
    }

    public static NewsFragment newInstance(ArrayList<NewsData> lastNewsData, ArrayList<NewsData> nextNewsData) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LAST_NEWS_ARG, lastNewsData);
        args.putParcelableArrayList(NEXT_NEWS_ARG, nextNewsData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            lastNews = getArguments().getParcelableArrayList(LAST_NEWS_ARG);
            nextNews = getArguments().getParcelableArrayList(NEXT_NEWS_ARG);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            //newsPresenter.
            newsPresenter.refreshData();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerNewsComponent.builder().appComponent(appComponent).newsModule(new NewsModule(this)).build().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        View header = inflater.inflate(R.layout.fragment_news_viewpager_lastnews, null);
        viewPagerLastNews = (ViewPager) header.findViewById(R.id.fragment_news_viewPager_lastNews);
        circlePageIndicator = (CirclePageIndicator) header.findViewById(R.id.fragment_news_indicator);
        View footer = inflater.inflate(R.layout.footer_progress, null);
        textViewFooter = (TextView) footer.findViewById(R.id.hataRaporu);

        lastNewsPagerAdapter = new LastNewsPagerAdapter(getChildFragmentManager());
        viewPagerLastNews.setAdapter(lastNewsPagerAdapter);
        newsAdapter = new NewsAdapter(getActivity(), picasso);


        circlePageIndicator.setViewPager(viewPagerLastNews);
        listViewNews.addHeaderView(header);
        listViewNews.addFooterView(footer);
        listViewNews.setAdapter(newsAdapter);

        if ((lastNews != null) && (!lastNews.isEmpty())) {
            lastNewsPagerAdapter.updateList(lastNews);
        }
        if ((nextNews != null) && !(nextNews.isEmpty())) {
            newsAdapter.updateList(nextNews);
        }

        listViewNews.setOnScrollListener(this);
        newsPresenter.onCreate();

        listViewNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), NewsDetailActivity.class).putExtra("newsId", ((NewsData) parent.getItemAtPosition(position)).getId()).putExtra("categoryId", ((NewsData) parent.getItemAtPosition(position)).getCategory().getId()).putExtra("categoryName", ((NewsData) parent.getItemAtPosition(position)).getCategory().getName()));
            }
        });

        newsPresenter.setViewPagerAutoSlide(viewPagerLastNews.getCurrentItem());

        viewPagerLastNews.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                newsPresenter.updateViewPagerPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPagerLastNews.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    newsPresenter.shouldStop(false);
                } else {
                    newsPresenter.shouldStop(true);
                }
                return false;
            }
        });

        /*viewPagerLastNews.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getGestureDetector().onTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    newsPresenter.shouldStop(false);
                } else {
                    newsPresenter.shouldStop(true);
                }
                return super.onTouch(v, event);
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                if(viewPagerLastNews.getCurrentItem() == 0){
                    newsPresenter.updateViewPagerPosition(8);
                    viewPagerLastNews.setCurrentItem(8);
                    newsPresenter.shouldStop(true);
                }
            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                if(viewPagerLastNews.getCurrentItem() == 9){
                    newsPresenter.updateViewPagerPosition(0);
                    viewPagerLastNews.setCurrentItem(0);
                    newsPresenter.shouldStop(true);
                }
            }
        });*/

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNewsFragmentInteractionListener) {
            mListener = (OnNewsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        newsPresenter.stopViewPagerAutoSlide();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int lastInScreen = firstVisibleItem + visibleItemCount;
        if ((lastInScreen >= totalItemCount) && !(loading)) {
            loading = true;
            textViewFooter.setVisibility(View.VISIBLE);
            newsPresenter.loadMoreNews(newsAdapter.getPageNumber());
        }
    }

    @Override
    public void showFooterProgress() {
        textViewFooter.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFooterProgress() {
        textViewFooter.setVisibility(View.GONE);
    }

    @Override
    public void addMoreData(ArrayList<NewsData> newsData) {
        newsAdapter.addMoreData(newsData);
        newsAdapter.updatePageNumber(newsAdapter.getPageNumber() + 1);
        loading = false;
    }

    @Override
    public void error(String errorData) {
        new AlertDialog.Builder(getActivity()).setMessage(errorData).setTitle("Uyarı").create().show();
    }

    @Override
    public void changeViewPagerItemToNext(int toPosition) {
        viewPagerLastNews.setCurrentItem(toPosition);
    }

    @Override
    public void updateLastNews(ArrayList<NewsData> newsDatas) {
        lastNewsPagerAdapter.updateList(newsDatas);
    }

    @Override
    public void updateNextNews(ArrayList<NewsData> newsDatas) {
        newsAdapter.updateFirstTenData(newsDatas);
    }

    @Override
    public void addNewData(NewsData newsData) {
        newsAdapter.addNewData(newsData);
    }


    public interface OnNewsFragmentInteractionListener {

    }


}
