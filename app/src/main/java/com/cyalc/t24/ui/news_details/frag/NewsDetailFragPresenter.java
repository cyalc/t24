package com.cyalc.t24.ui.news_details.frag;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsDetailFragPresenter {
    void getNewsById(String id);
    void unsubscribe();
}
