package com.cyalc.t24.ui.news_details.main;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsDetailPresenter {
    void getCategoryIdList(String categoryId);
    void setChosenItem(String newsId);
}
