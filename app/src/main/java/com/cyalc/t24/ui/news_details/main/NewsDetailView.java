package com.cyalc.t24.ui.news_details.main;

import java.util.ArrayList;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public interface NewsDetailView {
    void showProgress();
    void hideProgress();
    void error(String error);
    void initilazeViewPager(ArrayList<String> idList);
    void setChosenNews(int position);
}
