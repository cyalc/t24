package com.cyalc.t24.data.rest;

import com.cyalc.t24.data.rest.pojos.Category;
import com.cyalc.t24.data.rest.pojos.News;
import com.cyalc.t24.data.rest.pojos.NewsSingle;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public interface NewsService {
    @GET("/api/v3/stories.json")
    Observable<News> getNewsListByPage(@Query("paging") int pageNumber);

   // @GET("/api/v3/stories.json")
   // Ob

    @GET("/api/v3/categories.json?type=story")
    Observable<Category> getCategories();

    @GET("/api/v3/stories.json")
    Observable<News> getNewsListByCategoryId(@Query("category") String categoryId);

    @GET("/api/v3/stories.json")
    Observable<NewsSingle> getNewsById(@Query("story") String id);
}
