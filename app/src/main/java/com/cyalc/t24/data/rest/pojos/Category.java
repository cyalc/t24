package com.cyalc.t24.data.rest.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class Category {
    private boolean result;
    private ArrayList<NewsCategory> data = new ArrayList<>();

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ArrayList<NewsCategory> getData() {
        return data;
    }

    public void setData(ArrayList<NewsCategory> data) {
        this.data = data;
    }
}
