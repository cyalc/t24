package com.cyalc.t24.data.rest.pojos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class NewsImages implements Parcelable{
    private String list;
    private String box;
    private String page;
    private String grid;

    protected NewsImages(Parcel in) {
        list = in.readString();
        box = in.readString();
        page = in.readString();
        grid = in.readString();
    }

    public static final Creator<NewsImages> CREATOR = new Creator<NewsImages>() {
        @Override
        public NewsImages createFromParcel(Parcel in) {
            return new NewsImages(in);
        }

        @Override
        public NewsImages[] newArray(int size) {
            return new NewsImages[size];
        }
    };

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(list);
        dest.writeString(box);
        dest.writeString(page);
        dest.writeString(grid);
    }
}
