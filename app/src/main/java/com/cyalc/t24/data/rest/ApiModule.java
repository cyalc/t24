package com.cyalc.t24.data.rest;

import com.google.gson.Gson;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
@Module
public class ApiModule {
    public static final HttpUrl PRODUCTION_API_URL = HttpUrl.parse("http://t24.com.tr//");

    @Provides
    @Singleton
    HttpUrl provideBaseUrl() {
        return PRODUCTION_API_URL;
    }

    @Provides
    @Singleton
    @Named("Api")
    OkHttpClient provideApiClient(OkHttpClient client) {
        return createApiClient(client);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(HttpUrl baseUrl, OkHttpClient client, Gson gson) {
        return new Retrofit.Builder() //
                .client(client) //
                .baseUrl(baseUrl) //
                .addConverterFactory(GsonConverterFactory.create(gson)) //
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()) //
                .build();
    }

    @Provides @Singleton NewsService provideNewsService(Retrofit retrofit) {
        return retrofit.create(NewsService.class);
    }

    static OkHttpClient createApiClient(OkHttpClient client) {
        client = client.clone();
        return client;
    }
}
