package com.cyalc.t24.data.repositories;

import com.cyalc.t24.data.rest.NewsService;
import com.cyalc.t24.data.rest.pojos.Category;
import com.cyalc.t24.data.rest.pojos.News;
import com.cyalc.t24.data.rest.pojos.NewsCategory;
import com.cyalc.t24.data.rest.pojos.NewsSingle;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class NewsRepositoryImpl implements NewsRepository {

    private NewsService newsService;

    public NewsRepositoryImpl(NewsService newsService) {
        this.newsService = newsService;
    }

    @Override
    public Observable<News> getLastNews() {
        return newsService.getNewsListByPage(1);
    }

    @Override
    public Observable<News> getNextNews(int pageNumber) {
        return newsService.getNewsListByPage(pageNumber);
    }

    @Override
    public Observable<Category> getCategories() {
        return newsService.getCategories();
    }

    @Override
    public Observable<News> getNewsByCategoryId(String id) {
        return newsService.getNewsListByCategoryId(id);
    }

    @Override
    public Observable<NewsSingle> getNewsById(String id) {
        return newsService.getNewsById(id);
    }


}
