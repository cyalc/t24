package com.cyalc.t24.data.rest.pojos;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsSingle {
    private boolean isResult;
    private NewsDetail data;

    public boolean isResult() {
        return isResult;
    }

    public void setIsResult(boolean isResult) {
        this.isResult = isResult;
    }

    public NewsDetail getData() {
        return data;
    }

    public void setData(NewsDetail data) {
        this.data = data;
    }
}
