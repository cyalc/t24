package com.cyalc.t24.data.rest.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */

public class NewsData implements Parcelable {
    private String id;
    private String title;
    private String excerpt;
    private String alias;
    private NewsUrls urls;
    private NewsCategory category;
    private NewsStats stats;
    private NewsImages images;
    private Date publishingDate;

    public NewsData() {

    }

    protected NewsData(Parcel in) {
        id = in.readString();
        title = in.readString();
        excerpt = in.readString();
        alias = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(excerpt);
        dest.writeString(alias);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewsData> CREATOR = new Creator<NewsData>() {
        @Override
        public NewsData createFromParcel(Parcel in) {
            return new NewsData(in);
        }

        @Override
        public NewsData[] newArray(int size) {
            return new NewsData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public NewsUrls getUrls() {
        return urls;
    }

    public void setUrls(NewsUrls urls) {
        this.urls = urls;
    }

    public NewsCategory getCategory() {
        return category;
    }

    public void setCategory(NewsCategory category) {
        this.category = category;
    }

    public NewsStats getStats() {
        return stats;
    }

    public void setStats(NewsStats stats) {
        this.stats = stats;
    }

    public NewsImages getImages() {
        return images;
    }

    public void setImages(NewsImages images) {
        this.images = images;
    }

    public Date getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(Date publishingDate) {
        this.publishingDate = publishingDate;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof NewsData)) {
            return false;
        }

        return this.id.equals(((NewsData) o).getId());
    }
}
