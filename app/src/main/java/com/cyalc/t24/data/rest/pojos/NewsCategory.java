package com.cyalc.t24.data.rest.pojos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class NewsCategory implements Parcelable{
    private String id;
    private String name;
    private String alias;

    public NewsCategory(String id, String name, String alias){
        this.id = id;
        this.name = name;
        this.alias = alias;
    }

    protected NewsCategory(Parcel in) {
        id = in.readString();
        name = in.readString();
        alias = in.readString();
    }

    public static final Creator<NewsCategory> CREATOR = new Creator<NewsCategory>() {
        @Override
        public NewsCategory createFromParcel(Parcel in) {
            return new NewsCategory(in);
        }

        @Override
        public NewsCategory[] newArray(int size) {
            return new NewsCategory[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(alias);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
