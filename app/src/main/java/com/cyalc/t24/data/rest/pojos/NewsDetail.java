package com.cyalc.t24.data.rest.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by cagriyalcinsoy on 22/11/15.
 */
public class NewsDetail {
    private String text;
    private String id;
    private String title;
    private String excerpt;
    private String alias;
    private NewsUrls urls;
    private NewsCategory category;
    private NewsStats stats;
    private NewsImages images;
    private Date publishingDate;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public NewsUrls getUrls() {
        return urls;
    }

    public void setUrls(NewsUrls urls) {
        this.urls = urls;
    }

    public NewsCategory getCategory() {
        return category;
    }

    public void setCategory(NewsCategory category) {
        this.category = category;
    }

    public NewsStats getStats() {
        return stats;
    }

    public void setStats(NewsStats stats) {
        this.stats = stats;
    }

    public NewsImages getImages() {
        return images;
    }

    public void setImages(NewsImages images) {
        this.images = images;
    }

    public Date getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(Date publishingDate) {
        this.publishingDate = publishingDate;
    }
}
