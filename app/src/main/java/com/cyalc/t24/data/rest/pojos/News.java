package com.cyalc.t24.data.rest.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class News {
    private ArrayList<NewsData> data = new ArrayList<>();
    private Paging paging;
    private boolean result;



    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ArrayList<NewsData> getData() {
        return data;
    }

    public void setData(ArrayList<NewsData> data) {
        this.data = data;
    }
}
