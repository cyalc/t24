package com.cyalc.t24.data.rest.pojos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public class NewsUrls implements Parcelable{
    private String web;

    protected NewsUrls(Parcel in) {
        web = in.readString();
    }

    public static final Creator<NewsUrls> CREATOR = new Creator<NewsUrls>() {
        @Override
        public NewsUrls createFromParcel(Parcel in) {
            return new NewsUrls(in);
        }

        @Override
        public NewsUrls[] newArray(int size) {
            return new NewsUrls[size];
        }
    };

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(web);
    }
}
