package com.cyalc.t24.data.repositories;

import com.cyalc.t24.data.rest.NewsService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
@Module
public class RepositoryModule {
    @Provides
    @Singleton
    public NewsRepository provideNewsRepository(NewsService newsService){
        return new NewsRepositoryImpl(newsService);
    }
}
