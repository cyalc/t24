package com.cyalc.t24.data.repositories;

import com.cyalc.t24.data.rest.pojos.Category;
import com.cyalc.t24.data.rest.pojos.News;
import com.cyalc.t24.data.rest.pojos.NewsSingle;

import rx.Observable;

/**
 * Created by cagriyalcinsoy on 21/11/15.
 */
public interface NewsRepository {
    Observable<News> getLastNews();
    Observable<News> getNextNews(int pageNumber);
    Observable<Category> getCategories();
    Observable<News> getNewsByCategoryId(String id);
    Observable<NewsSingle> getNewsById(String id);
}
