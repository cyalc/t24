package com.cyalc.t24;

import com.cyalc.t24.data.DataModule;
import com.cyalc.t24.data.repositories.NewsRepository;
import com.cyalc.t24.data.repositories.RepositoryModule;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 8.11.2015.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                RepositoryModule.class,
                DataModule.class
        }
)
public interface AppComponent {
    void inject(App app);
    NewsRepository getNewsRepositoey();

    Picasso getPicasso();
}

